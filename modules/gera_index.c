#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "gera_index.h"

Args processa_argumento(int, char const *[]);


int main(int argc, char const *argv[]) {
  system("clear");
  Args args;
  int opcao;

  args = processa_argumento(argc, argv);

  return 0;
}


Args processa_argumento(int argc, char const *argv[]){
  Args args;

  if( !strcmp(argv[(argc - 1)], "-write") )
    args.flag = 1;
  else if( !strcmp(argv[(argc - 1)], "-read") )
    args.flag = 2;

  return args;
}
