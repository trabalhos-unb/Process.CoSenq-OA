#include <stdio.h>
#include <stdlib.h>

#include "unb_system.h"

char captura_opcao_menu(void);
void inserir_aluno(void);

int main(int argc, char const *argv[]) {
  char opcao;
  do {
    opcao = captura_opcao_menu();
    switch( opcao ){
      case '1':
        //system("./gera_index.exe -write ");
        inserir_aluno();
        getchar(); getchar();
        break;
      case '2':
        break;
      case '3':
        break;
      case '4':
        break;
      case '5':
        break;
      case '6':
        /*Sai do programa*/
        break;
    }
  } while( opcao != '6');
  return 0;
}

void mostra_opcao_menu(void){
  system("clear");

  printf("    _    _       ____     _____           _                      \n");
  printf("   | |  | |     |  _ \\   / ____|         | |                    \n");
  printf("   | |  | |_ __ | |_) | | (___  _   _ ___| |_ ___ _ __ ___       \n");
  printf("   | |  | | '_ \\|  _ <   \\___ \\| | | / __| __/ _ \\ '_ ` _ \\ \n");
  printf("   | |__| | | | | |_) |  ____) | |_| \\__ \\ ||  __/ | | | | |   \n");
  printf("    \\____/|_| |_|____/  |_____/ \\__, |___/\\__\\___|_| |_| |_| \n");
  printf("                                 __/ |                           \n");
  printf("                                |___/                            \n\n");
  printf("Sistema de Notas UnB v0.1 beta\n");
  printf("1. Inserir novo aluno na UnB\n");
  printf("2. Jubilar um aluno da UnB\n");
  printf("3. Dar nota ao aluno numa disciplina\n");
  printf("4. Relatório\n");
  printf("5. Buscar\n");
  printf("6. Sair\n");
}

char captura_opcao_menu(void){
  char option = '0';
  do{
    /*system("clear"); debug */
    mostra_opcao_menu();
    printf("--> ");
    scanf("%c", &option);
  }while( option < '1' || option > '6' );
  return option;
}

void inserir_aluno_arquivo(char const *nome, Lista1 aluno){
  FILE *fp = fopen(nome ,"r");
  int maior_id;
  char id[4], *pEnd;

  if( fp == NULL ){
    printf("Não foi possível abrir o arquivo\n");
  }else{
    /*Vai ate o ultimo ID*/
    fseek(fp,0,SEEK_END);
    while( fgetc(fp) != 'D') fseek(fp,-2,SEEK_CUR);
    fseek(fp,1,SEEK_CUR);
    fscanf(fp,"%3s", id);
    maior_id = (int) strtol(id, &pEnd,10);
    maior_id++;


    fclose(fp);
  }
}

void inserir_aluno(void){
    Lista1 aluno;
    /*
    //Le matricula aluno
    scanf("%6s", aluno.matricula);
    //Le nome aluno
    scanf("%13s", aluno.nome);
    //Le idade aluno
    scanf("%d", &aluno.idade);
    //Le tamano aluno
    scanf("%c", &aluno.tamanho);
    //Le par_caracter aluno
    scanf("%2s", aluno.par_caracter);
    */
    inserir_aluno_arquivo("index_lista1.txt", aluno);
}
